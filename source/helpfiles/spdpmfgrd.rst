*********
spdpmfgrd
*********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdpmfgrd  -o <String> -i <String>  [--gdal <string>] [--class  <uint_fast16_t>] [--image]  [--medianfilter <uint_fast16_t>]  [--nomedian] [--grd <float>]  [--maxelev <float>] [--initelev  <float>] [--slope <float>]  [--maxfilter <uint_fast16_t>]  [--initfilter <uint_fast16_t>]  [--overlap <uint_fast16_t>] [-b  <float>] [-c <unsigned int>] [-r  <unsigned int>] [--] [--version]  [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input SPD file.
--gdal <string>  Provide the GDAL driver format (Default ENVI), Erdas Imagine is HFA,  KEA is KEA
--class <uint_fast16_t>  Only use points of particular class
--image  If set an image of the output surface will be generated rather than  classifying the points (useful for debugging and parameter selection)
--medianfilter <uint_fast16_t>  Size of the median filter (half size i.e., 3x3 is 1) (Default 2)
--nomedian  Do not run a median filter on generated surface (before classifying  ground point or export)
--grd <float>  Threshold for deviation from identified ground surface for classifying  the ground returns (Default 0.3)
--maxelev <float>  Maximum elevation difference threshold (Default 5)
--initelev <float>  Initial elevation difference threshold (Default 0.3)
--slope <float>  Slope parameter related to terrain (Default 0.3)
--maxfilter <uint_fast16_t>  Maximum size of the filter (Default 7)
--initfilter <uint_fast16_t>  Initial size of the filter (note this is half the filter size so a 3x3  will be 1 and 5x5 will be 2) (Default 1)
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Classifies the ground returns using the progressive morphology  algorithm: **spdpmfgrd**