*********
spdmccgrd
*********
 
SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdmccgrd  -o <String> -i <String>  [--thresofchangemultireturn]  [--class <uint_fast16_t>]  [--median] [--thresofchange  <float>] [--filtersize  <uint_fast16_t>] [--interpnumpts  <uint_fast16_t>] [--interpmaxradius  <float>] [--stepcurvetol <float>]  [--mincurvetol <float>]  [--initcurvetol <float>]  [--scalegaps <float>]  [--numofscalesbelow  <uint_fast16_t>]  [--numofscalesabove  <uint_fast16_t>] [--initscale  <float>] [--overlap  <uint_fast16_t>] [-b <float>] [-c  <unsigned int>] [-r <unsigned int>]  [--] [--version] [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output SPD file.
-i <String>, --input <String>  (required)  The input SPD file.
--thresofchangemultireturn  Use only multiple return pulses to calculate the amount of change  between iterations.
--class <uint_fast16_t>  Only use points of particular class
--median  Use a median filter to smooth the generated raster instead of a (mean)  averaging filter.
--thresofchange <float>  The threshold for the  (Default = 0.1)
--filtersize <uint_fast16_t>  The size of the smoothing filter (half size i.e., 3x3 is 1; Default =  1).
--interpnumpts <uint_fast16_t>  The number of points used for the TPS interpolation (Default = 16)
--interpmaxradius <float>  Maximum search radius for the TPS interpolation (Default = 20)
--stepcurvetol <float>  Iteration step curveture tolerance parameter (Default = 0.5)
--mincurvetol <float>  Minimum curveture tolerance parameter (Default = 0.1)
--initcurvetol <float>  Initial curveture tolerance parameter (Default = 1)
--scalegaps <float>  Gap between increments in scale (Default = 0.5)
--numofscalesbelow <uint_fast16_t>  The number of scales below the init scale to be used (Default = 1)
--numofscalesabove <uint_fast16_t>  The number of scales above the init scale to be used (Default = 1)
--initscale <float>  Initial processing scale, this is usually the native resolution of the  data.
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Classifies the ground returns using the multiscale curvature algorithm:  **spdmccgrd**