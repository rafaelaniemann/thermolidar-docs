********
spdmerge
********
 
SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdmerge  -o <String> [--keepextent] [-s  <std::string>] [--classes  <uint_fast16_t>] ...  [--returnIDs  <uint_fast16_t>] ...  [--source]  [--ignorechecks] [-c] [-r  <std::string>] [-p <std::string>]  [--wavebitres <8BIT|16BIT|32BIT>]  [-x <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>] -f <SPD|ASCIIPULSEROW|ASCII|FWF_DAT|DECOMPOSED_DAT|LAS|LASNP|LASSTRICT|DECOMPOSED_COO|ASCIIMULTILINE>  [--] [--version] [-h] <std::string>  ...``


Where
-----
-o <String>, --output <String>  (required)  The output SPD file.
--keepextent  When indexing the file use the extent of the input file as the minimum  extent of the output file.
-s <std::string>, --schema <std::string>  A schema for the format of the file being imported (Note, most  importers do not require a schema)
--classes <uint_fast16_t>  (accepted multiple times)  Lists the classes for the files listed.
--returnIDs <uint_fast16_t>  (accepted multiple times)  Lists the return IDs for the files listed.
--source  Set source ID for each input file
--ignorechecks  Ignore checks between input files to ensure compatibility
-c, --convert_proj  Convert file buffering to disk
-r <std::string>, --output_proj <std::string>  WKT std::string representing the projection of the output file
-p <std::string>, --input_proj <std::string>  WKT std::string representing the projection of the input file
--wavebitres <8BIT|16BIT|32BIT>  The bit resolution used for storing the waveform data (Default: 32BIT)
-x <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>, --indexfield <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>  The location used to index the pulses
-f <SPD|ASCIIPULSEROW|ASCII|FWF_DAT|DECOMPOSED_DAT|LAS|LASNP|LASSTRICT|DECOMPOSED_COO|ASCIIMULTILINE>, --inputformat <SPD|ASCIIPULSEROW|ASCII|FWF_DAT|DECOMPOSED_DAT|LAS|LASNP|LASSTRICT|DECOMPOSED_COO|ASCIIMULTILINE>  (required)  Format of the input file
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.
<std::string>  (accepted multiple times)  (required)  The list of input files


NAME
====
Merge compatable files into a single non-indexed SPD file: **spdmerge**