************
spdtranslate
************

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdtranslate  -o <String> -i <String>  [--keepextent] [--pulseversion  <unsigned int>] [--pointversion  <unsigned int>] [--wavenoise  <float>] [--Oz <float>] [--Oy  <double>] [--Ox <double>]  [--defineOrigin] [--tly <double>]  [--tlx <double>] [--defineTL]  [--keeptemp] [--convert_proj]  [--output_proj <string>]  [--input_proj <string>] [-b  <float>] [-c <unsigned int>] [-r  <unsigned int>] [-s <string>] [-t  <string>] [--scan] [--polar]  [--spherical] [--wavebitres <8BIT|16BIT|32BIT>] [-x <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>] --of <SPD|UPD|ASCII|LAS|LAZ> --if <SPD|ASCIIPULSEROW|ASCII|FWF_DAT|DECOMPOSED_DAT|LAS|LASNP|LASSTRICT|DECOMPOSED_COO|ASCIIMULTILINE> [--] [--version]  [-h]``


Where
-----
-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input file.
--keepextent  When indexing the file use the extent of the input file as the minimum  extent of the output file.
--pulseversion <unsigned int>  Specify the pulse version to be used within the SPD file (Default: 2)
--pointversion <unsigned int>  Specify the point version to be used within the SPD file (Default: 2)
--wavenoise <float>  Waveform noise threshold (Default 0)
--Oz <float>  Origin Z coordinate
--Oy <double>  Origin Y coordinate
--Ox <double>  Origin X coordinate.
--defineOrigin  Define the origin coordinate for the SPD.
--tly <double>  Top left Y coordinate for defining the SPD file index.
--tlx <double>  Top left X coordinate for defining the SPD file index.
--defineTL  Define the top left (TL) coordinate for the SPD file index
--keeptemp  Keep the tempory files generated during the conversion.
--convert_proj  Convert file buffering to disk
--output_proj <string>  WKT string representing the projection of the output file
--input_proj <string>  WKT string representing the projection of the input file
-b <float>, --binsize <float>  Bin size for SPD file index (Default 1)
-c <unsigned int>, --numofcols <unsigned int>  Number of columns within a tile (Default 0), using this option  generats a non-sequencial SPD file.
-r <unsigned int>, --numofrows <unsigned int>  Number of rows within a tile (Default 25)
-s <string>, --schema <string>  A schema for the format of the file being imported (Note, most  importers do not require a schema)
-t <string>, --temppath <string>  A path were temporary files can be written too
--scan  Index the pulses using a scan coordinate system
--polar  Index the pulses using a polar coordinate system
--spherical  Index the pulses using a spherical coordinate system
--wavebitres <8BIT|16BIT|32BIT>  The bit resolution used for storing the waveform data (Default: 32BIT)
-x <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>, --indexfield <FIRST_RETURN|LAST_RETURN|START_WAVEFORM|END_WAVEFORM|ORIGIN|MAX_INTENSITY|UNCHANGED>  The location used to index the pulses (Default: UNCHANGED)
--of <SPD|UPD|ASCII|LAS|LAZ>  (required)  Format of the output file (Default SPD)
--if <SPD|ASCIIPULSEROW|ASCII|FWF_DAT|DECOMPOSED_DAT|LAS|LASNP|LASSTRICT|DECOMPOSED_COO|ASCIIMULTILINE>  (required)  Format of the input file (Default SPD)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.

NAME
====
Convert between file formats: **spdtranslate**