**********
spdtileimg
**********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdtileimg  {-m|-c} [-i <String>] [-t  <String>] [-o <String>] [-w  <String>] [-b <double>] [-r  <double>] [-f <String>] [--]  [--version] [-h]``


Where
-----
-m, --mosaic  (OR required)  Mosaic the images (within the input list) together.

**-- or --**
-c, --clump  (OR required)  Create a clumps image specifying the location of the  tiles.

-i <String>, --input <String>  The text file with a list of input files.
-t <String>, --tiles <String>  The input XML file defining the tiles.
-o <String>, --output <String>  The output image.
-w <String>, --wkt <String>  A file containing the WKT string representing the projection (--clump  only).
-b <double>, --background <double>  The output image background value (--mosaic only).
-r <double>, --resolution <double>  The output image pixel size (--clump only).
-f <String>, --format <String>  The output image format.
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Tools for mosaicing raster results following tiling: **spdtileimg**