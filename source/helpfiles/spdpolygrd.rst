**********
spdpolygrd
**********

SPDLib 3.1.0, Copyright (C) 2013 Sorted Pulse Library (SPD) This program comes with ABSOLUTELY NO WARRANTY. This is free software, and you are welcome to redistribute it under certain conditions; See website (http://www.spdlib.org). Bugs are to be reported on the trac or directly to spdlib-develop@lists.sourceforge.net

USAGE
=====
``spdpolygrd  {--global|--local} -o  <String> -i <String> [--class  <uint_fast16_t>] [--iters <int>]  [--degree <int>] [--grdthres  <float>] [-b <float>] [--overlap  <uint_fast16_t>] [-c <unsigned  int>] [-r <unsigned int>] [--]  [--version] [-h]``


Where
-----
--global  (OR required)  Classify negative height as ground

**-- or --**
--local  (OR required)  Remove falsely classified ground returns using plane  fitting

-o <String>, --output <String>  (required)  The output file.
-i <String>, --input <String>  (required)  The input SPD file.
--class <uint_fast16_t>  Only use points of particular class (Ground is class == 3, Default is  All classes)
--iters <int>  Number of iterations for polynomial surface to converge on ground  (Default = 2).
--degree <int>  Order of polynomial surface (Default = 1).
--grdthres <float>  Threshold for how far above the interpolated ground surface a return  can be and be reclassified as ground (Default = 0.25).
-b <float>, --binsize <float>  Bin size for processing and output image (Default 0) - Note 0 will use  the native SPD file bin size.
--overlap <uint_fast16_t>  Size (in bins) of the overlap between processing blocks (Default 10)
-c <unsigned int>, --blockcols <unsigned int>  Number of columns within a block (Default 0) - Note values greater  than 1 result in a non-sequencial SPD file.
-r <unsigned int>, --blockrows <unsigned int>  Number of rows within a block (Default 100)
--ignore_rest  Ignores the rest of the labeled arguments following this flag.
--version  Displays version information and exits.
-h, --help  Displays usage information and exits.


NAME
====
Classify ground returns using a surface fitting algorithm: **spdpolygrd**