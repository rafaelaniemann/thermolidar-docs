Data Analysis
=============

This section provides the tools for analysis and interpretation of results. Once thermal and LiDAR data have been processed in the previous sections, the user can generate the mapping needed to interpret the physiological state of the forest mass analysed.

First, the user has a set of data obtained in the field of physiology which are analysed and grouped, using the tools available at the module Health Condition Level.

From the tools available in the structurally homogeneous forest units module, the user can perform a preliminary classification of the stands, based on structural homogeneity. This factor is important because thermal values behave differently according to the structure of objects.

Finally, from the Forest Heath classification tools the user has the necessary tools to perform a classification based on the thermal values for the various homogeneous units. To improve the classification, the user can define training plots according to data collected in the field of physiology, visually are established different levels of affection.

.. figure:: ../../images/analysis/workflow_analysis.png
   :width: 700px
   :figwidth: 700px
   :align: center

Health Condition Levels
-----------------------

Shapiro Test
~~~~~~~~~~~~

Before proceeding with the classification of items by level of damage according to several variables taken in the field, we verify that the set of physiological variables follow a normal distribution. For this we use the Shapiro test, located in the toolbox [Analysis] Health Condition Level > Shapiro Test.

.. figure:: ../../images/analysis/shapiro.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Input vector**: Vector file that contains information on physiological data. 
* **Parameter**: Vector's field to analyse if it follows the normal distribution

Ouput Parameters
````````````````
	
* **R Console Output**: File with the output result. The output is composed of the following values
	* Statistic - The value of the Shapiro-Wilk statistic.
	* p.value - An approximate p-value for the test. This is said in Royston (1995) to be adequate for p.value < 0.1.
	* Method - The character string "Shapiro-Wilk normality test".
	* data.name - A character string giving the name(s) of the data.

Interpretation
~~~~~~~~~~~~~~

The null-hypothesis of this test is that the population is normally distributed. Thus if the p-value is less than the chosen alpha level, then the null hypothesis is rejected and there is evidence that the data tested are not from a normally distributed population. In other words, the data is not normal. On the contrary, if the p-value is greater than the chosen alpha level, then the null hypothesis that the data came from a normally distributed population cannot be rejected. E.g. for an alpha level of 0.05, a data set with a p-value of 0.02 rejects the null hypothesis that the data are from a normally distributed population. However, since the test is biased by sample size, the test may be statistically significant from a normal distribution in any large samples. 


Standardize
~~~~~~~~~~~

To be able to use the variables in our analysis, they should follow a normal distribution. One way to force these follow a normal distribution is by definition. This characterization involves the conversion of the variable that follows a distribution N (μ, σ), a new variable with distribution N (1,0). This tool is situated in [Analysis] Heath condition level > Standardize.

.. figure:: ../../images/analysis/standardize.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Input vector**: Shape that contains information on physiological data. 
* **Variable**: Shapefile's field to standardize

Ouput Parameters
````````````````
	
* **Output vector**: The user will output a new shapefile with the standardized variable.
	

Clustering
~~~~~~~~~~

This tool allows us to group one or more physiological variables according to their degree of similarity between individuals in the sample. So, the goal of clustering is to determine the intrinsic grouping in a set of unlabelled data. But how to decide what constitutes a good clustering? It can be shown that there is no absolute *best* criterion which would be independent of the final aim of the clustering. Consequently, it is the user which must supply this criterion, in such a way that the result of the clustering will suit their needs.

To make this tool has been chosen by a hierarchical approach. The user-supplied items are categorized into levels and sublevels within a class hierarchy, forming a hierarchical tree structure.

.. figure:: ../../images/analysis/clustering.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Input vector**: Shape containing information on physiological data. 
* **Cols names**: Name the columns containing details physiology (separated by semicolons ';')
* **Number of groups**: Number of groups

Ouput Parameters
````````````````

	* **R plots**: File with the R output result.
	* **Output vector**: Output shapefile with a new variable (group) indicating that each group is member.
	
ANOVA
~~~~~

ANOVA test are conducted for each variable to indicate how well the variable discriminates between clusters.

The hypothesis is tested in the ANOVA is that the population means (the average of the dependent variable at each level of the independent variable) are equal. If the population means are equal, it means that the groups did not differ in the dependent variable, and consequently, the independent variable is independent of the dependent variable.

.. figure:: ../../images/analysis/anova.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Input vector**: Shapefile that contains information about the cluster
* **Dependent variable**: Field shape that acts as a dependent variable
* **Independent variable**: Field shape that acts as an independent variable

Ouput Parameters
````````````````

* **R Console Output**: File with the R output result. The result will be a list of ANOVA tables, one for each response (even if there is only one response". They have columns "Df", "Sum Sq", "Mean Sq", as well as "F value" and "Pr(>F)" if there are non-zero residual degrees of freedom. There is a row for each term in the model, plus one for "Residuals" if there are any.

Interpretation
~~~~~~~~~~~~~~

If the critical level associated with the F statistics (i.e., the probability of obtaining values as obtained or older), is less than 0.05, we reject the hypothesis of equal means and conclude that not all the population means being compared are equal. Otherwise, we cannot reject the hypothesis of equality and we cannot claim that the groups being compared differ in their population averages.	

Structurally Homogeneous Forest Units
-------------------------------------

The analytical purpose of this tool is the definition of structurally homogeneous stands that allow us to minimize the effects of structure on the thermal information, and therefore allow us to obtain related health outcomes woodland. To do this, the software allows the user to define the structure of the stand from the height data. The calculation of uniformity is therefore a function of two variables directly derived from LiDAR data the 95th percentile obtained from MDV and the penetration rate, obtained from density points that penetrate the forest canopy.

.. figure:: ../../images/analysis/SHFU.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Polygons**: Vector file that contains polygons to classify.
* **ID**: Vector's field that that indicates the id of each item
* **Equation**: `Operator used to classify the feats <http://thermolidar-docs.readthedocs.org/en/latest/source/files/shfu_eq/>`_. 
* **Clusters**: Number of output groups. The value is 3 by default.
* **Cutoff**: Stop threshold algorithm. The value is 0.5 by default.

Ouput Parameters
````````````````

* **Output**: Vector file name containing the classification.

Forest Health Classification
----------------------------

Unsupervised Pixel-based Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The user has a stratification of the study area, and classified based on the structure. Through this tool, a classification of the pixels of temperatures will be performed, based on the classification of defined structure. Thus, the output will be a raster temperature for each of the groups of homogeneity.

.. figure:: ../../images/analysis/UP_classification.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Temperature raster layer**: Input temperature raster 
* **SHFU Polygons**: Vector layer that contains structurally homogeneous stands.
* **SHFU Field**: SHFU layer field containing the group that belong each item.
* **Clusters**: Number of output classes.
* **Cutoff**: Stop threshold. The value is 0.5 by default.

Ouput Parameters
````````````````

* **Output**: Raster file name containing the classification of temperatures based on homogeneous stand units.
* **Statistics**: CSV File containing statistics for groups.
	
Unsupervised Object-oriented Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The user has a stratification of the study area, and classified based on the structure. Through this tool, a classification of the objects of temperatures mean will be performed, based on the classification of defined structure. Thus, the output will be a raster temperature for each of the groups of homogeneity.

.. figure:: ../../images/analysis/UO_classification.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Temperature raster layer**: Input temperature raster 
* **SHFU Polygons**: Vector layer that contains structurally homogeneous forest units.
* **SHFU Field**: SHFU layer field containing the group that belongs each item.
* **Clusters**: Number of output classes.
* **Cutoff**: Stop threshold. The value is 0.5 by default.

Ouput Parameters
````````````````

* **Output**: Raster file name containing the classification of temperatures based on homogeneous stand units.
* **Statistics**: CSV File containing statistics for groups.
	
Supervised Pixel-based Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Similarly as in the previous point, the user can perform a classification of the temperature response to the stand of homogeneity. Unlike supervised classification, the user has a number of AOIs that guide the classification process.

.. figure:: ../../images/analysis/SP_classification.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Temperature raster layer**: Input temperature raster 
* **SHFU Polygons**: Vector layer that contains structurally homogeneous stands.
* **SHFU Field**: SHFU layer field containing the group that belong each item.
* **ROIs vector**: Vector file with training areas.
* **ROIs - SHFU Identifier**: Vector's field that contains the group's homogeneity that belongs each item of training areas.
* **ROIs - FSC Identifier**: Vector's field that contains the group "Forest health level" that belongs within their group of homogeneity.
* **k**: Number k nearest neighbours. The value is 3 by default.

Ouput Parameters
````````````````

	* **Output**: Raster file name containing the classification of temperatures based on homogeneous stand units.
	* **Statistics**: CSV File containing statistics for groups.
	
Supervised Object-oriented Classification
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. figure:: ../../images/analysis/SO_classification.jpg
   :width: 450px
   :figwidth: 700px
   :align: center
	
Required Input Parameters
`````````````````````````

* **Temperature raster layer**: Input temperature raster 
* **SHFU Polygons**: Vector layer that contains structurally homogeneous stands.
* **SHFU Field**: SHFU layer field containing the group that belong each item.
* **ROIs vector**: Vector file with training areas.
* **ROIs - SHFU Identifier**: Vector's field that contains the group's homogeneity that belongs each item of training areas.
* **ROIs - FSC Identifier**: Vector's field that contains the group "Forest health level" that belongs within their group of homogeneity.
* **k**: Number k nearest neighbours. The value is 3 by default.

Ouput Parameters
````````````````

* **Output**: Raster file name containing the classification of temperatures based on homogeneous stand units.
* **Statistics**: CSV File containing statistics for groups.

