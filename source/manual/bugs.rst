Known bugs
==========

There are some known bugs related to SPDLib tools. Some of them have been reported in the `SPDLib distribution list <http://sourceforge.net/p/spdlib/mailman/spdlib-develop/>`_.
 #. *spdmetrics* tool crashes with particular values of metrics in Windows systems (`thread <http://sourceforge.net/p/spdlib/mailman/message/32854707/>`_)
 #. *spdmetrics* does not work with vector output (in both Linux and Windows systems, see `thread <http://sourceforge.net/p/spdlib/mailman/message/32223926/>`_)
 #. SPD Windows files are bigger than what they should be
 #. SPD Linux files are not read in Windows systems
