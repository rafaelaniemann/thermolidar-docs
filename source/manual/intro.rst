Introduction
============

This section summarize the description of the software and the practical application of the tools implemented using thermal and LiDAR data collected in the framework of THREMOLIDAR project. 

The main structure of the software is summarized in this figure.

.. figure:: ../../images/tutorial/general_flowchart.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Detail of the main processes carried out by the software


Software structure
------------------

Processing
~~~~~~~~~~

This package includes tools for conducting the processing of raw Thermal and LiDAR data in order to obtain the products required to achieve the parametric analysis of forest health assessment.

   * **A.1. Lidar processing**. LiDAR data tool set for the generation of DSMs, DTMs, DVMs and vegetation statistical derivatives thereof.

   * **A.2. Thermal image processing**. Thermal data tool set for the calibration of RAW airborne thermal imaging. In addition, tools provide the possibility of calculating a derivate indicator using the difference of the air temperature minus the crown temperature.


Data Analysis
~~~~~~~~~~~~~

This package includes tools for conducting the simultaneous analysis of thermal and LiDAR data information linked to field data measurements to evaluate the state and trends of forest health. A detailed description of data inputs and processes applied is included in Fig. 2

   * **B.1. Forest Stand Segmentation (FSS)**. The processed image is decomposed into regions or objects. Object based delineation algorithms are applied with this tool to define forest stands unis for further study.

   * **B.2. Health condition levels (HCL)**. Different physiological indicators from field data measurements are processed with this tool to define the ground truth condition of forest status. Health condition levels are statistically generated based in clustering and subsequently validated by ANOVA.

   * **B.3. Structurally homogeneous forest units (SHFU)**. This option provides the tools required for the classification of forest stands structurally different. The data applied to perform this classification is defined by the user. In this report the main average height of the trees estimated based on LiDAR data has been applied as input data. Alternatively, user can provide external forest maps in a .shp format type with an attribute of the number of class.

   * **B.3. Forest Health Monitoring (FHM)**. The final result of this toolkit is the classification of forest health condition levels.  This process requires the training sample plots analysed and classified in step 1, the forest stand classification calculated in step 2 (or externally provided) and the thermal image data.  Then, the user can apply a supervised classification of thermal data using the training sample plots classified in different health condition levels and based on a specific structural stand level. Optionally, users can apply a non-supervised classification in case the absence of field data measurements.
