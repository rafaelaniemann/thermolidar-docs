LiDAR Processing
================

LiDAR data is often provided as a number of tiles or flight lines. Depending on computers capacity, in some cases it might be convenient to merge flight files into a single file, or to divide data in overlapping tiles with an appropriate size. 

Due to |thld| software uses SPDLib suite for LiDAR data manipulation, data has to be converted into the native SPDLib file format. This means that LiDAR data must be converted into **Sorted Pulse Data format** (**SPD**) from **LAS** files, which is the file standard for the interchange of LASer data recommended by the American Society for Photogrammetry and Remote Sensing (ASPRS_). Noisy data can be eventually removed before being processed. Once the SPD files have been obtained, ground points are classified and then point heights relative to the ground are inferred. Ground and no-ground points are interpolated to generate DTM, DSM and CHM. From height information a range of metrics mainly applied to forestry applications -but not only- can be derived. Here below this workflow is depicted.

.. figure:: ../../images   /lidar/WorkFlow.png
   :width: 550px
   :figwidth: 700px
   :align: center
	
   LiDAR workflow using the SPDLib toolset. In this case, SPD format files are supplied as input. Names of SPDLib commands are highlighted in bold font (i.e. **spdmerge**, **spdtranslate**, **spddeftiles**, etc.). Pink boxes represent output products. [Bunting2013]_

Convert between formats
-----------------------

Since LiDAR processing modules make use of SPDLib tools, the first step is to convert the input dataset into SPD files. There are two types of SPD files, non-indexed and indexed. A format translation module has been included in QGIS to this purpose. The module allows the conversion between different formats and it is also used to re-project data.

.. figure:: ../../images/thld_toolbox/spdtranslate.png
   :width: 350px
   :figwidth: 700px
   :align: center

   The module for format conversion is located in the *LiDAR* submenu of the |thld| Toolbox


|thld| plug-in supports  SPD/UPD, LAS and a wide range of ASCII formats but the current level of support is not intended to encompass all the available formats. For more information about the formats SPDLib supports, please see the :doc:`Supported File Formats section <formats>`.

Reprojection
~~~~~~~~~~~~

It is possible to define the projection of the SPD file explicitly using the *input* and *output projection*. Both options expect a text file containing the **WKT** (*Well Known Text*) string representing the projection information. In order to change projection, the input projection option is not required, but if it is known, then it should be advantageous to provide it.

Memory Requirements
~~~~~~~~~~~~~~~~~~~

When converting to an UPD very little memory is required as only a few pulses are held in memory at any one time, this is because no sorting of the pulses is required. On the other hand when generating an SPD file the data needs to be spatially sorted. Therefore, the whole file is read into memory and sorted into the spatial grid before being written output the file. This requires enough memory to store the whole dataset and index data structure in memory. If memory is not sufficient to complete this operation the file needs to be split into blocks to fit into memory.

The option to select splitting the file to disk while building the SPD file is *temporal path* which is the path and base file name while the tiles will be written. The *num rows* parameter specifies the number of rows of the final SPD file that will be written to each temporary tile. Note that the tile height in is *binsize* x *num rows* (in units the data is projected). The *num. columns* option maybe set where datasets are very wide such that the tiles are not the full width of the output file. Whether this option is used, the final SPD file will result in a non-sequential rather than a sequential file. This means the data on disk is not order left-to-right top-to-bottom or top-left to bottom-right, which has some performance benefits. Obviously, allowing the SPD file to be built in stages is slower but once completed it is faster to make spatial queries within the file. Besides, other processing steps (i.e., classification and interpolations) can be applied to the whole file with only relatively small memory requirements.

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdtranslate.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface to convert between different data formats

Required Input Parameters
`````````````````````````
* **Input**: SPD file that contains the LiDAR point clouds.
* **Index**: The location used to index the pulses and points (required):
	* **FIRST_RETURN**
	* **LAST_RETURN**
* **Input Format**: Format of the input file (Default SPD).
	* **SPD**: SPD input format with or without spatial index
	* **ASCII**: ASCII input format
	* **LAS/LAZ**: Both zipped or normal LAS input format
	* **LASNP**: LAS input without pulse information
* **Ouput Format**: Format of the output file (Default SPD).
	* **SPD**: SPD output format
	* **UPD**: SPD output format without spatial index
	* **ASCII**: ASCII output format
	* **LAS**: LAS output format
	* **LAZ**: Zipped LAS output format

Optional Input Parameters
`````````````````````````
* **Binsize**: (*float*) Bin size for SPD file index (Default 1)
* **Schema**: (*string*) schema for the format of the ASCII file being imported 
* **Input Projection**: (*string*) WKT string representing the projection of the input file 
* **Output Projection**: (*string*) WKT string representing the projection of the output file
* **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
* **Num. Rows**: (*integer*) Number of rows within a block (Default 25)
* **Temporal Path**: (*string*) Path where temporary files can be written to.

Output Parameters
`````````````````
* **Output**: The output SPD file

Merge files
-----------

In some situations it might be convenient to merge various files into a single SPD file. The merging module merges compatible files into a single non-indexed SPD file. It is possible to provide the projection information of the output file and input files if known. 

This module allows displaying classes and returns IDs of the input files with list **returns IDs** and **list classes** options, respectively. The **ignore checks** option forces the input files to be merged in case files come from different sources or have different bin sizes.

.. figure:: ../../images/thld_toolbox/spdmerge.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module for merging files is located in the *LiDAR* submenu of the |thld| toolbox 

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdmerge.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface to merge compatible files into a single non-indexed SPD file

Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds (accept multiple files separated by comas).
* **Index**: The location used to index the pulses and points (required):

	* **FIRST_RETURN**
	* **LAST_RETURN**

* **Input Format**: Format of the input file (Default SPD).

	* **SPD**: SPD input format with or without spatial index
	* **ASCII**: ASCII input format
	* **LAS/LAZ**: Both zipped or normal LAS input format
	* **LASNP**: LAS input without pulse information

Optional Input Parameters
`````````````````````````

* **List Returns IDs**: (*list of files*) Lists the return IDs for the files listed (accept multiple files separated by comas).
* **List Classes**: (*list of files*) Lists the classes for the files listed (accept multiple files separated by comas).
* **Keep Extent**: (*Yes/No*) Use the extent of the input files as the minimum extent of the output file when indexing the file.
* **Source ID**: (*Yes/No*) Set source ID for each input file
* **Ignore Checks**: (*Yes/No*) Ignore checks between input files to ensure compatibility
* **Schema**: (*string*) schema for the format of the ASCII file being imported 
* **Input Projection**: (*string*) WKT string representing the projection of the input file 
* **Output Projection**: (*string*) WKT string representing the projection of the output file

Output Parameters
`````````````````

* **Output**: The output SPD file

Split data into tiles
---------------------

LiDAR data is supplied as flight lines or tiles with different shapes and sizes. It is always useful to divide laser data into equally sized square tiles, though. This helps to store, manage and access data easily. Single tiles should meet memory requirements in order to reduce computational times, which determines the maximum size of each file given an average point density. Overlapping zones between tiles help to prevent border errors and guarantee continuous raster models.

|thld| has a built-in tool to create tiles given **tiles size** and the **overlap**. Output tiles are saved into the **output path** (this includes path and *prefix*) and are named as ``_rowYYcolXX.spd``, being ``YY`` and ``XX`` the number of row and column of the corresponding tile. Tiles definition is stored in an output XML file (**output xml**) containing their column, row, extent and core extent (tile extent without overlap). 

.. code-block:: xml

   <tiles columns="23" overlap="50" rows="16" xmax="256500" xmin="239343.510" xtilesize="750" ymax="707224.300" ymin="695246.440" ytilesize="750">
      <tile col="1" corexmax="240093.510" corexmin="239343.510" coreymax="695996.440" coreymin="695246.440" file="" row="1" xmax="240143.510" xmin="239293.510" ymax="696046.440" ymin="695196.440"/>
      <tile col="2" corexmax="240843.510" corexmin="240093.510" coreymax="695996.440" coreymin="695246.440" file="" row="1" xmax="240893.510" xmin="240043.510" ymax="696046.440" ymin="695196.440"/>

      ...

      <tile col="22" corexmax="255843.510" corexmin="255093.510" coreymax="707246.440" coreymin="706496.440" file="" row="16" xmax="255893.510" xmin="255043.510" ymax="707296.440" ymin="706446.440"/>
      <tile col="23" corexmax="256593.510" corexmin="255843.510" coreymax="707246.440" coreymin="706496.440" file="" row="16" xmax="256643.510" xmin="255793.510" ymax="707296.440" ymin="706446.440"/>
   </tiles>


The module supports to create single tiles (**SINGLE** option) by supplying **row** and **column**, or to generate the complete set of tiles (**ALL** option).

The module also creates an auxiliary file listing the input LiDAR which can be eventually kept (**keep file list**) once the module has finished. It might happen that some tiles are empty; in that case those files can be removed enabling the **delete tiles** option. 

.. figure:: ../../images/thld_toolbox/spdtiling.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module for tiling LiDAR data is within the *LiDAR* submenu of the |thld| toolbox 

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdtiling.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface for tiling a set of SPD files 

Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds (accept multiple files separated by comas).

Optional Input Parameters
`````````````````````````

* **Extract Tiles**: Where to extract
	* **ALL**: Create all tiles
	* **SINGLE**: Extract an individual tile given its row and column
* **Delete Tiles**: (*Yes/No*) If shapefile exists delete it and then run
* **Keep File List**: (*Yes/No*) Keep auxiliary file containing a list of the input files to be tiled
* **Tile Size**: (*float*) Size (in units of the coordinate system) of the square tiles (Default: 1000)
* **Output Path**: (*string*) The output XML file that contains the tiles definition

.. * **Overlap Size**: (*float*) Size (in units of coordinate systems) of the overlap for tiles (Default 100)
.. * **Column**: (*integer*) The column of the tile to be extracted (only with single)
.. * **Row**: (*integer*) The row of the tile to be extracted (only with single)

Output Parameters
`````````````````

* **Output XML**: The output XML file that contains the tiles definition

Remove Noise
------------

Many factors may introduce errors in LiDAR point clouds, including water vapour clouds, multipath, poor equipment calibration, or even a flock of birds. In order to avoid further errors and artefacts in final digital models and poor assess of height metrics, those points have to be removed.

This module removes vertical noise from LiDAR datasets by means of three different. Upper and lower **absolute thresholds** will clip the file to fit these values. **Relative threshold** will remove, for each bin within a SPD file, points outside the upper and lower values relative to the median height. Whilst **global threshold** will use the whole SPD file to calculate the median height and remove points relative to it.

.. figure:: ../../images/thld_toolbox/spdrmnoise.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module for removing noise from data is located in the *LiDAR* submenu of the |thld| toolbox 

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdtiling.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface for tiling a set of SPD files 

Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds.

Optional Input Parameters
`````````````````````````

* **Global Rel. Upper Threshold**: (*float*) Global relative to median upper threshold for returns which are to be removed
* **Global Rel. Lower Threshold**: (*float*) Global relative to median lower threshold for returns which are to be removed
* **Relative Upper Threshold**: (*float*) Relative to median upper threshold for returns which are to be removed
* **Relative Lower Threshold**: (*float*) Relative to median lower threshold for returns which are to be removed
* **Absolute Upper Threshold**: (*float*) Absolute upper threshold for returns which are to be removed
* **Absolute Lower Threshold**: (*float*) Absolute lower threshold for returns which are to be removed

.. * **Column**: (*integer*) The column of the tile to be extracted (only with single)
.. * **Row**: (*integer*) The row of the tile to be extracted (only with single)

Output Parameters
`````````````````

* **Output**: The output SPD file without noise


Classify Ground Returns
-----------------------

Two different classification algorithms have been implemented into the plug-in. These algorithms also called filters allow the classification of the LiDAR points, identifying which point belong to the ground. The filters implement the *Progressive Morphology* (Zhang et al., 2003; [Zhang2003]_) and the *Multiscale Curvature* (Evans and Hudak, 2007; [EvansHudak2007]_) methodologies.

Progressive Morphology filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
To classify ground returns an implementation of the *Progressive Morphology* algorithm (**PMF**) has been provided. The algorithm QGIS interface has only three options to be set. Under most circumstances the default parameters will be fit the purpose and it is recommended to use the simplest parameters configuration given by default.

The **class** option allows to apply the filter to particular classes (i.e., if ground returns have been already classified but they need tidying up).

.. figure:: ../../images/thld_toolbox/spdpmfgrd.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module that implements the PMF algorithm to classify ground is within the *LiDAR* submenu of the |thld| toolbox


Parameters
``````````

.. figure:: ../../images/lidar/Module_spdpmfgrd.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface of the *Progressive Morphology Algorithm* to classify ground points

Required Input Parameters
.........................

* **Input**: SPD file that contains the LiDAR point clouds.

Optional Input Parameters
.........................

* **Binsize**: (*float*) Bin size for SPD file index (Default 1)
* **Class**: (*integer*) Only use points of particular class
* **Ground Threshold**: (*float*) Threshold for deviation from identified ground surface for classifying  the ground returns (Default 0.3)
* **Median Filter**: (*integer*) Size of the median filter (half size i.e., 3x3 is 1) (Default 2)
* **No Median**: (Yes/No) Do not run a median filter on generated surface
* **Max. Elevation**: (*float*) Maximum elevation difference threshold (Default 5)
* **Initial Elevation**: (*float*) Initial elevation difference threshold (Default 0.3)
* **Slope**: (*float*) Slope parameter related to terrain (Default 0.3)
* **Max. Filter**: (*float*) Maximum size of the filter (Default 7)
* **Initial Filter**: (*float*) Initial size of the filter (half size i.e., 3x3 is 1)  (Default 1)

.. * **Overlap**: (*integer*) Size (in bins) of the overlap between processing blocks (Default 10)
.. * **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
.. * **Num. Rows**: (*integer*) Number of rows within a block (Default 25)

Output Parameters
.................

* **Output**: The output SPD file containing classification


Multiscale Curvature filter
~~~~~~~~~~~~~~~~~~~~~~~~~~~

The plug-ins integrates an implementation of the *Multiscale Curvature* algorithm (**MCC**). As before, the QGIS interface has only three options to be set: input, output and class argument. Under most circumstances default parameters for the algorithm will be fit for purpose, but be careful that the bin size used within SPD is not too large as the processing will be at this resolution.

.. figure:: ../../images/thld_toolbox/spdmccgrd.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module that implements the MCC algorithm to classify ground is within the *LiDAR* submenu of the |thld| toolbox 

Parameters
``````````

.. figure:: ../../images/lidar/Module_spdmccgrd.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface of the *Progressive Morphology Algorithm* to classify ground points

Required Input Parameters
.........................

* **Input**: SPD file that contains the LiDAR point clouds.

Optional Input Parameters
.........................

* **Binsize**: (*float*) Bin size for SPD file index (Default 1)
* **Class**: (*integer*) Only use points of particular class
* **Median**: (*Yes/No*) Use a median filter to smooth the generated raster instead of a (mean) averaging filter.
* **Filter Size**: (*integer*) The size of the smoothing filter (half size i.e., 3x3 is 1; Default =  1)
* **Num. Points Tps**: (*integer*) The number of points used for the TPS interpolation (Default = 16)
* **Max. Radius Tps**: (*float*) Maximum search radius for the TPS interpolation (Default = 20)
* **Step Curve Tolerance**: (*float*) Iteration step curvature tolerance parameter (Default = 0.5)
* **Min. Curve Tolerance**: (*float*) Minimum curvature tolerance parameter (Default = 0.1)
* **Initial Curve Tolerance**: (*float*) Initial curvature tolerance parameter (Default = 1)
* **Scale Gaps**: (*float*) Gap between increments in scale (Default = 0.5)
* **Num. Scales Below**: (*integer*) The number of scales below the init scale to be used (Default = 1)
* **Num. Scales Above**: (*integer*) The number of scales above the init scale to be used (Default = 1)
* **Initial Scale**: (*float*) Initial processing scale, this is usually the native resolution of the data.
* **Max. Elevation Threshold**: (*float*) Maximum elevation difference threshold (Default 5)
* **Initial Elevation Threshold**: (*float*) Initial elevation difference threshold (Default 0.3)
* **Slope**: (*float*) Slope parameter related to terrain (Default 0.3)
* **Max. Filter**: (*float*) Maximum size of the filter (Default 7)
* **Initial Filter**: (*float*) Initial size of the filter (half size i.e., 3x3 is 1)  (Default 1)

.. * **Overlap**: (*integer*) Size (in bins) of the overlap between processing blocks (Default 10)
.. * **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
.. * **Num. Rows**: (*integer*) Number of rows within a block (Default 25)

Output Parameters
.................

* **Output**: The output SPD file containing classification


Filter points depending on class
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The **class** option applies the filter to returns of a particular class (i.e., ground returns). This represents a way to improve the ground return classification is to combine more than one filtering algorithm to take advantage of their particular strengths and weaknesses. In fact, a particularly useful combination is to first run the PMF algorithm where a thick slice is taken (e.g., 1 or 2 metres above the raster surface) and then the MCC is applied to find the ground returns (setting the **class** option to 3).

It can be also useful for TLS as it can take a thick slice with MCC algorithm and then use the PMF algorithm to tidy that result up to get a good overall ground classification.

.. _spddefheight:

Normalise heights
-----------------

SPD files supports both elevation corresponding to a vertical datum and an above-ground height for each discrete return. Before data can be used for generating a Canopy Height Model (CHM) or any height related metric, height field has to be populated. This can be done in two ways. The simplest way is to use a DTM of the same resolution as the SPD file bin size (**Image** option). The disadvantage of using a DTM is that it is if the DTM is not accurate it can introduce some artefacts. Using this method the only parameters are the input files, both LiDAR file and the DTM, and an output file. The raster DTM needs to the same resolution as the SPD grid and it can be any raster format supported by the GDAL library.

The other option is to interpolate a value for each point generating a continuous surface and reducing any artefacts (**Interpolate** option). The recommended approach for the interpolation is to use the Natural Neighbour method, as demonstrated by Bater and Coops (2009; [BaterCoops2009]_).

.. figure:: ../../images/thld_toolbox/spddefheight.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module that defines the height above ground is located in the *LiDAR* submenu of the |thld| toolbox 

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spddefheight.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface of the module to define points heights from the ground


Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds.

Optional Input Parameters
`````````````````````````

* **Reference Surface**: Select the reference surface for normalise heights
   * **Interpolation**
   * **Image** 
* **Elevation**: (*raster*) The input elevation image
* **Binsize**: (*float*) Bin size for SPD file index (Default 1)
* **Interpolator**: Different interpolation methods to choose from
	* **Natural Neighbour**
	* **Nearest Neighbour**
	* **TIN Plate**

.. * **Thin**: (*Yes/No*) Thin the point cloud when interpolating
.. * **Thin Resolution**: (*float*) Resolution of the grid used to thin the point cloud
.. * **Point per Bin**: (*integer*) The number of point allowed within a grid cell following thinning
.. * **Overlap**: (*integer*) Size (in bins) of the overlap between processing blocks (Default 10)
.. * **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
.. * **Num. Rows**: (*integer*) Number of rows within a block (Default 25)

Output Parameters
`````````````````

* **Output**: The output SPD file

Interpolation Module
--------------------

The most common products that can be created from a LiDAR dataset are Digital Terrain Models (DTMs), Digital Surface Models (DSMs) and Canopy Height Models (CHMs). To produce those products, it is necessary to interpolate a raster surface from the classified ground returns and top surface points. Create Digital Model within the |thld| plug-in permits to generate these products by choosing **model** option. 

A key parameter is the output raster resolution or **binsize** which needs to be a multiple of the SPD input file spatial index. Different interpolators can be selected with the **interpolator** option. This module supports many raster formats by means of the GDAL library.

.. figure:: ../../images/thld_toolbox/spdinterpolate.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The interpolation module is in the *LiDAR* submenu of the |thld| toolbox 

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdinterp.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface to interpolate data

Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds.
* MODEL: 
	* **DTM**: Digital Terrain Model
	* **MDS**: Digital Surface Model
	* **CHM**: Canopy Height Model

Optional Input Parameters
`````````````````````````

* **Binsize**: (*float*) Bin size for SPD file index (Default 1)
* **Interpolator**: Different interpolation methods to choose from
	* **Natural Neighbour**
	* **Nearest Neighbour**
	* **TIN Plate**

.. * **Overlap**: (*integer*) Size (in bins) of the overlap between processing blocks (Default 10)
.. * **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
.. * **Num. Rows**: (*integer*) Number of rows within a block (Default 25)

Output Parameters
`````````````````
* **Output**: The raster file containing the interpolated model

Examples
~~~~~~~~

Digital Surface Models (DSMs) are easily done by setting **model** to **DSM**. The module will perform an interpolation of the elevation information of all the points of the LiDAR file. The result is a surface representing the ground and all the objects attached to it as can be seen in figure below.

.. figure:: ../../images/lidar/qgis_visualization/QGIS_dsm.png
   :width: 700px
   :figwidth: 700px
   :align: center

   Example of visualization in grey scales of a DSM (1m resolution) generated with the |thld| plug-in

In case ground returns have been classified, then interpolating elevation information of ground points will generate a Digital Terrain Model (DTM). For this purpose **model** has to be set to **DTM**. The output raster represents the bare ground surface (see next figure).

.. figure:: ../../images/lidar/qgis_visualization/QGIS_dtm.png
   :width: 700px
   :figwidth: 700px
   :align: center

   Example of visualization in grey scales of a DTM (1m resolution) generated with the |thld| plug-in

In a forest environment, those points not classified as ground are commonly classified as vegetation. To interpolate the height above ground information of vegetation points produces a Canopy Height Model (CHM). In this case, the **model** option is set to **CHM**. The result is raster where canopies are perfectly depicted and ground (see figure below).

.. figure:: ../../images/lidar/qgis_visualization/QGIS_chm.png
   :width: 700px
   :figwidth: 700px
   :align: center

   Example of visualization in grey scales of a CHM (1m resolution) generated with the |thld| plug-in


Generate metrics
----------------

|thld| plug-in is able to calculated different metrics at the same time. Metrics can be simple statistical moments, percentiles of point heights, or even count ratios. Mathematical operators can also be applied to either other metrics or operators.

The XML file required by the **metrics** option has to be defined a priori with a hierarchical list of metrics and operators. Metrics can be defined manually by typing each XML file which allows the user to adequate the XML file to particular purposes (for more information about XML metrics files, see the :doc:`How to define metrics? <metrics>` document. However, the user can generate the XML file automatically by means of the module `Create metrics XML file`_.

The module supports different output data formats, that is, **raster** (all GDAL formats) and **vector**. **Raster** option extends the output to the entire input file, assessing the metrics for each pixel the final raster output and creating as many bands as metrics have been defined within the XML file. **Vector** option requires an input shapefile containing polygon entities. The output shapefile database will be populated with the metrics computed inside the polygons.

.. figure:: ../../images/thld_toolbox/spdmetrics.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module to generate metrics is located in the *LiDAR* submenu of the |thld| toolbox

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spdmetrics.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface to calculate metrics

Required Input Parameters
`````````````````````````

* **Input**: SPD file that contains the LiDAR point clouds.
* **Metrics**: (*file*) XML file containing the metrics template
* **Output Data Format**:
	* **Image**: Raster output
	* **Vector**: Vector output

Required Input Parameters
`````````````````````````

* **Binsize**: (*float*) Bin size for processing and the resolution of the output image. Note: 0 will use the native SPD file bin size 
* **Vector File**: (*shapefile*) Input shapefile (only with vector output).

.. * **Num. Columns**: (*integer*) Number of columns within a block (Default 0) - Note values greater than 1 result in a non-sequential SPD file.
.. * **Num. Rows**: (*integer*) Number of rows within a block (Default 25)

Output Parameters
`````````````````

	* **Output**: The raster file containing the interpolated model

The figure below shows the Height 95th percentile computed for the same dataset than the previous examples:

.. figure:: ../../images/lidar/qgis_visualization/QGIS_metrics_pH95.png
   :width: 700px
   :figwidth: 700px
   :align: center

   Height 95th percentile computed into a raster image (10m resolution)



Create metrics XML file
-----------------------

The module offers the possibility to create the file with a single metric selected from a list. However, the list also accept the options **ALL**, **FOREST** and **PERCENTILES**:

* **ALL**: Selects all the metrics listed in the option list.
* **FOREST**: Includes some metrics that are commonly used in forest applications. This metrics includes percentiles from 99th to 50th, *groundCover*, *canopyCover*, *maxHeight* and *meanHeight*.
* **PERCENTILES**: Includes all percentiles from 99th to 10th.

In case other metrics than those available in the list are needed, the user can specify them by writing their names separated by **;** (semi-colon, and without spaces) in the field labelled with **string format**. This could be an example::

   pH99;pH98;pH95;pH90;pH80;maxHeight;canopyCover;NumberReturnsNoGround

In this case, the module will omit any selection made in the first input parameter and it will create the XML file with the metrics supplied by the user. In case the string any metric is misspelled, the module will inform the user of the mistake and omit the metric. The file will be created with the metrics that are correctly written. 

.. figure:: ../../images/thld_toolbox/spdmetrics.png
   :width: 450px
   :figwidth: 700px
   :align: center

   The module to generate metrics XML file is located in the *LiDAR* submenu of the |thld| toolbox

Parameters
~~~~~~~~~~

.. figure:: ../../images/lidar/Module_spddefmetrics.png
   :width: 450px
   :figwidth: 700px
   :align: center

   Interface of the module to generate XML file containing metrics

Optional Input Parameters
`````````````````````````

* **List of metrics**: Metrics to be used
* **String of metrics**: Metrics to be used

Output Parameters
`````````````````

* **Output**: The output XML file containing metric definitions

.. raw:: html

   <h2>References</h2>

.. [Bunting2013] Bunting, P., Armston, J., Clewley, D., Lucas, R. M., 2013. Sorted pulse data (SPD) library. Part II: A processing framework for LiDAR data from pulsed laser systems in terrestrial environments. Computers and Geosciences 56, 207 -- 215.

.. [BaterCoops2009] Bater, C. W., Coops, N. C., 2009. Evaluating error associated with lidar-derived DEM interpolation. Computers and Geosciences 35 (2), pp. 289–300.

.. [EvansHudak2007] Evans, J. S., Hudak, A. T., 2007. A multi-scale curvature algorithm for classifying discrete return lidar in forested environments. IEEE Transactions on Geoscience and Remote Sensing 45 (4), pp. 1029 -- 1038.

.. [Naesset1997a] Naesset, E., 1997. Determination of mean tree height of forest stands using airborne laser scanner data. ISPRS Journal of Photogrammetry and Remote Sensing, 52, pp. 49 -- 56.

.. [Naesset1997b] Naesset, E., 1997. Estimating timber volume of forest stands using airborne laser scanner data. Remote Sensing of Environment, 61, pp. 246 -- 253.

.. [Naesset2002] Naesset, E., 2002. Predicting forest stands characteristics with airborne scanning laser using a practical two-stage procedure and field data. Remote Sensing of Environment, 80, pp. 88 -- 99.

.. [Zhang2003] Zhang, K., Chen, S., Whitman, D., Shyu, M., Yan, J., Zhang, C., 2003. A progressive morphological filter for removing nonground measurements from airborne LIDAR data. IEEE Transactions on Geoscience and Remote Sensing 41 (4), pp. 872 -- 882.


.. _ASPRS: http://www.asprs.org/Committee-General/LASer-LAS-File-Format-Exchange-Activities.html
.. _LibLAS: http://www.liblas.org
.. _GDAL: http://www.gdal.org
