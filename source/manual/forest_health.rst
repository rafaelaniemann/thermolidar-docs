Forest Health Assessment
========================
Forest Health assessment module consists of four different tools: Forest Stand Segmentation (FSS), Health Condition Level (HCL), Structurally Homogeneous Forest Units (SHFU) and Forest Health Monitoring (FHM). The following figure summarizes the main structure of this module and the input required throughout the process.

.. figure:: ../../images/tutorial/FHA.png
   :width: 450px
   :figwidth: 700px
   :align: center
	
Forest stands segmentation
--------------------------
Within OD tools, users are willing to choose between developing a semi-automatic segmentation and using a pre-defined object feature. Segmentation tools are based on algorithms that segment an image into areas of connected pixels based on the pixel DN value. ThermoLiDAR image segmentation tools will be based on region growing algorithms. The basic approach of a region growing algorithm is to start from a seed region (typically one or more pixels) that are considered to be inside the object to be segmented. The pixels neighbouring this region are evaluated to determine if they should also be considered part of the object. If so, they are added to the region and the process continues as long as new pixels are added to the region. Region growing algorithms vary depending on the criteria used to decide whether a pixel should be included in the region or not, the type connectivity used to determine neighbours, and the strategy used to visit neighbouring pixels. Image segmentation is a crucial step within the object-based remote sensing information retrieval process. As a step prior to classification the quality assessment of the segmentation result is of fundamental significance for the recognition process as well as for choosing the appropriate approach and parameters for a given segmentation task. Alternatively, user could be interested on using a pre-defined object feature. This object feature could be a segmentation shape file provided from other source or any other land cover mapping. Also, the user can use a pre-defined regular object, defining the size of the square to be used previously.

.. figure:: ../../images/tutorial/example_FSS.png
   :width: 700px
   :figwidth: 700px
   :align: center
	
Forest Condition Levels
-----------------------
The most critical part in applying forest health condition indicators is the user's accuracy defining forest degradation levels. Besides user's training another critical factor is to select under analysis a robust physiological indicator and to carry out an accurate field measurements campaign. 

Potential physiological indicators of forest decline such us pigment concentration, photosynthesis, respiration and transpiration rate holds great potential to shed light on the mechanisms and processes that occur as a result of drought stress. In the short-term, climate can change the physiological conditions of the forest resulting in acute damage, but chronic exposure usually results in cumulative effects on physiological process. These factors effects on the plants light reactions or enzymatic functions and increased respiration from reparative activities. Gradual decreases in photosynthesis, stomatal conductance, carbon fixation, water use efficiency, resistance to insect and cold resistance were found in most of trees which are very typical symptom of stress conditions

Long-term exposure of water stress to a combination of high light levels and high temperatures causes a depression of photosynthesis and photosystem II efficiency that is not easily reversed, even for water-stress-resistant forest species. The decrease in the photochemical efficiency of photosystem II (ΦPSII) is related to the conversion of violaxanthin to antheraxanthin and zeaxanthin produced by an increase in harmless non-radiative energy dissipation (qN) and providing photo-protection from oxidative damage. One of the most widely physiological indicator applied in the analysis of long-term effect on forest health condition is de Leaf Area Index (LAI). The following is an example of the statistical analysis performs on LAI values measured from an Oak forest inventoried in the framework of THERMOLIDAR project.

.. figure:: ../../images/tutorial/example_FCL.png
   :width: 700px
   :figwidth: 700px
   :align: center

Structurally Homogeneous Forest Units
-------------------------------------
This tool provides the tools required for the classification of forest stands structurally different. The classification is based on two main structural parameters, average height of the trees and density. Input data needed to run this process is obtained from LiDAR data. Alternatively, user can provide external forest maps in a .shp format type with an attribute of the number of class. The following figure shows an example of the units defined for the oak forest under analysis. Using a grey scale, trees were grouped in 3 classes with significant differences in terms of structural composition.

.. figure:: ../../images/tutorial/example_SHFU.png
   :width: 700px
   :figwidth: 700px
   :align: center
	
Forest Health Monitoring
------------------------
The main function of this tool is defining health condition differences in the vegetation at the stand level. Input parameters defined by users should mainly contain: thermal imaging data and the FSC Polygons (vector file with structurally homogeneous stands. Forest stands included in this analysis should be carried specifically based on one species.  The user can perform a supervised or an unsupervised classification depending of the availability of field data measurements to define training areas.  It should be highlight, that at this point of the analysis, users are willing to obtain an integrated mapping of forest health distribution levels based on thermal data but also standardized by forest stands units defined from lidar-based metrics. The following figure, shows an example of the units defined for assessment of the status of forest condition. Using a colour palette, trees were grouped in different classes with significant differences in terms of structural composition and physiological status. The colour palette ranges from red to green, where red colour is relate with trees with high level of damage and green colour represents trees with optimum health condition.

.. figure:: ../../images/tutorial/example_FHM.png
   :width: 700px
   :figwidth: 700px
   :align: center