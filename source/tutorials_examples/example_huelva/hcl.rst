Health condition levels
~~~~~~~~~~~~~~~~~~~~~~~

We have the physiology data of Leaf Area Index (LAI) for 25 of the 216 trees inventoried in the area of Huelva, stored in the vector file:
**thld_training\\huelva\\Z1_holmOak\\FieldData\\LAI\\lai_huelva.shp**.

First, load the shape of polygons in QGIS (Layer - Add vector layer ...)

.. figure:: example_huelva/images/hcl_lai_plots.png
	:align: center
	
	LAI measurements in Huelva
	
Before proceeding with the classification of items by level of damage according to several variables taken in the field, we verify that the set of physiological variables follow a normal distribution. For this we use the Shapiro test, located in the toolbox **[Analysis] Health Condition Level > Shapiro Test.**

.. WARNING:: The first time you use these tools, you will need to start QGIS in administrator mode (R install required dependencies)

Shapiro Test
~~~~~~~~~~~~

.. figure:: ../../images/thld_toolbox/hcl_shapiro.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   **Shapire Test** is located in the "Health Condition Level" submenu of the THERMOLIDAR plugin toolbox 
	
* **Input vector**: Vector file that contains information on physiological data.
* **Var**: Vector’s field to analyze if it follows the normal distribution. In this case the parameter LAI

We introduce the parameters as shown in the following figure:
	
.. figure:: example_huelva/images/hcl_shapiro.png
	:align: center
	
	Interface of the “Shapiro Test” module
	
Obtaining the following output:

.. figure:: example_huelva/images/hcl_shaphiro_out.png
	:align: center

In the example, the p-value is much higher than 0.05, so we conclude that LAI data follow a normal distribution. In the case where p-value is less than 0.05 the data would be discarded, or these should be normalized.

In the case that the variable does not follow a normal distribution, it is necessary to standardize using the **[Analysis] Health Condition Level> Standarize**

Clustering
~~~~~~~~~~
This tool allows us to group one or more physiological variables according to their degree of similarity between individuals in the sample. In this case we will group by the variable LAI, we have previously verified that follows a normal distribution.
This tool creates many groups tool damage level depending on the specified physiological variable. In this case, we will select 3 levels as a function of LAI variable.
We can find the tool in **[Analysis] Health Condition Level > Clustering**

.. figure:: ../../images/thld_toolbox/hcl_clustering.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   **Clustering** is located in the “Health Condition Level” submenu of the THERMOLIDAR plugin toolbox 

We introduce the values as shown in the following figure:

.. figure:: example_huelva/images/hcl_clustering.png
	:align: center

The vector output file will be stored in the folder **thld_training\\huelva\\Z1_holmOak\\out\\hcl.shp** that will be used subsequently. Each of the trees will be classified as regions of interest to guide the supervised classification of the thermal image.

We obtain results in the dendrogram with the cluster of data:

.. figure:: example_huelva/images/hcl_clustering_out.png
	:align: center

LAI data grouped into 3 categories health condition, with the following results:

.. figure:: example_huelva/images/hcl_clustering_out2.png
	:align: center	

Finally, we must ensure that the groups are significantly different, according to the variables used. This is done through the tool **[Analysis] Heath condition level> ANOVA**.

ANOVA
~~~~~
.. figure:: ../../images/thld_toolbox/hcl_anova.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   **ANOVA** is located in the “Health Condition Level” submenu of the THERMOLIDAR plugin toolbox 
	
Selected as the dependent variable the group that owns each parcel; and as the dependent variable that we want to check if it is significant in the group.

.. figure:: example_huelva/images/hcl_anova.png
	:align: center

We get the following output:
	
.. figure:: example_huelva/images/hcl_anova_out.png
	:align: center
	
If the critical level associated with the F statistics (ie, the probability of obtaining values as obtained or older), is less than 0.05, we reject the hypothesis of equal means and conclude that not all the population means being compared are equal. Otherwise, we cannot reject the hypothesis of equality and we cannot claim that the groups being compared differ in their population averages.

