Structurally homogenous forest units
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The analytical purpose of this tool is the definition of structurally homogeneous stands that allow us to minimize the effects of structure on the thermal information, and therefore allow us to obtain related health outcomes woodland.

We start from the metric of the objects to be classified. This vector file was generated at the point of obtaining metric lidar **[Processing] LiDAR > Calculate metrics**.

* **thld_training\\huelva\\Z1_holmOak\\out\\metrics.shp**

The user must enter the equation by which you want to group the stands, for example according to the equation of the dominant height. In our case, we use the equation obtained from the 95th percentile. 

The tool used in this section is located in **[Analysis] Structurally homogeneus forest units**

.. figure:: ../../images/thld_toolbox/shfu.png
   :width: 285px
   :figwidth: 700px
   :align: center
   
   **SHFU** is located in the “Structuraly Homogeneous Forest Units” submenu of the THERMOLIDAR plugin toolbox 
	
In this example, the polygons will be classified into 3 different groups of homogeneity (Cluster parameter).

The Cutoff parameter determines the accuracy of the fit. A value of 0 is of higher precision and larger number of iterations will be needed to reach the final solution.
	
.. figure:: example_huelva/images/shfu.png
	:align: center
	
	Interface of the “SHFU” module

We get as output:

.. figure:: example_huelva/images/shfu_out.png
	:align: center
	
Now, we must assign the SHFU group to the objects (regions of interest) described on the Forest Health Level section. 

Thus we will use vector files:
* **thld_training\\huelva\\Z1_holmOak\\out\\hcl.shp**
* **thld_training\\huelva\\Z1_holmOak\\out\\shfu.shp**

We will use the tool **QGIS Geoalgorithms > Vector general tools > Join attributes table**

.. figure:: example_huelva/images/shfu_joinAttr.png
	:align: center

We insert the parameters as shown in the following figure:

.. figure:: example_huelva/images/shfu_joinAttr2.png
	:align: center
	
We will obtain a new output file (thld_training\\huelva\\Z1_holmOak\\out\\aois.shp) will use to guide the classification of the thermal image, are of interest to the following fields: 
* **SHFU.** Homogeneous group that the object owns. 
* **HCL.** Health condition level groups.

	
