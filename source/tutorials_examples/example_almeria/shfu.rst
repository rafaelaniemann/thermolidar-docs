Structurally homogenous forest units
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The analytical purpose of this tool is the definition of structurally homogeneous stands that allow us to minimize the effects of structure on the thermal information, and therefore allow us to obtain related health outcomes woodland.

The user must enter the equation by which you want to group the stands, for example according to the equation of the dominant height. In our case, we use the equation obtained from the 95th percentile. In this example, the polygons will be classified into 3 different groups of homogeneity.

.. figure:: example_almeria/images/shfu.png
	:align: center
	
	Interface of the “SHFU” module

We get as output:

.. figure:: example_almeria/images/shfu_out.png
	:align: center
	
