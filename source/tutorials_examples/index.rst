======================
Examples and tutorials
======================

.. toctree::
   :maxdepth: 2

   lidar_tutorial
   examples

.. toctree::
   :hidden:

   formats
   metrics
   allmetrics

.. .. toctree::
..    :hidden:

..    example_huelva/thermal_calibration
..    example_huelva/tc_ta
..    example_huelva/hcl
..    example_huelva/shfu
..    example_huelva/fhc
..    example_almeria/thermal_calibration
..    example_almeria/tc_ta
..    example_almeria/hcl
..    example_almeria/shfu
..    example_almeria/fhc







